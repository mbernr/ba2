from math import sqrt

from simulator.Config import Config


rules = list(open('quantum_tm.input', 'r'))

name = ""
initial = ""
accepting = ""
rejecting = ""
transitions = {}

current = None
for i in range(len(rules)):
    line = rules[i]
    if line.startswith("//"):
        continue
    elif line.startswith(("name")):
        name = line.replace("name:", "").strip()
    elif line.startswith(("init")):
        initial = line.replace("init:", "").strip()
    elif line.startswith(("accept")):
        accepting = line.replace("accept:", "").strip()
        transitions[accepting] = dict()
        transitions[accepting]['0'] = [(1, 'q_a', '0', '-')]
        transitions[accepting]['1'] = [(1, 'q_a', '1', '-')]
        transitions[accepting]['#'] = [(1, 'q_a', '#', '-')]
    elif line.startswith(("reject")):
        rejecting = line.replace("reject:", "").strip()
    else:
        x = line.strip().split(',')
        if len(x) == 2:
            current = (x[0],x[1])
        elif len(x) == 4:
            if current[0] not in transitions:
                transitions[current[0]] = dict()
            if current[1] not in transitions[current[0]]:
                transitions[current[0]][current[1]] = []
            transitions[current[0]][current[1]].append((eval(x[0]),x[1],x[2],x[3]))


_input = input().strip()
if _input == '':
    _input = '#'

print(transitions)

configs = [[]]
initial_config = Config(1, initial, 0, list(_input))
configs[0].append(initial_config)


while True:

    configs.append([])
    now = configs[-2]
    next = configs[-1]

    for config in now:
        print(config)
        print("-----")
    print("------------------------------")

    _ = input("ENTER to continue, 'q' to quit")

    if _ == "q" or _ == "Q":
        break

    all_done = True
    for config in now:
        if config.state != accepting:
            all_done = False
            break

    if all_done:
        break

    for config in now:
        try:

            # get transitions
            transitions_for_current_config = transitions[config.state][config.tape[config.head]]

            # compute new configs
            for transition in transitions_for_current_config:

                # change amplitude and state
                new_config = Config(config.amp * transition[0], transition[1], config.head, list(config.tape))

                # write
                new_config.tape[new_config.head] = transition[2]

                # move head
                if transition[3] == '<':
                    if new_config.head == 0:
                        new_config.tape.insert(0, '#')
                    else:
                        new_config.head -= 1
                elif transition[3] == '>':
                    if new_config.head == len(new_config.tape) - 1:
                        new_config.tape.append('#')
                    new_config.head += 1

                # add config
                next.append(new_config)

        except KeyError:
            break


