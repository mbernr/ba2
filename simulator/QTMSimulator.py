from simulator.Config import Config


def prepareTransitions(states, accepting, rejecting):
    transitions = {}
    for state in states:
        transitions[state] = {}
    transitions[accepting]['0'] = [(1, accepting, '0', '-')]
    transitions[accepting]['1'] = [(1, accepting, '1', '-')]
    transitions[accepting]['#'] = [(1, accepting, '#', '-')]
    transitions[rejecting]['0'] = [(1, accepting, '0', '-')]
    transitions[rejecting]['1'] = [(1, accepting, '1', '-')]
    transitions[rejecting]['#'] = [(1, accepting, '#', '-')]
    return transitions


class QTMSimulator:

    def __init__(self, states, initial, accepting, rejecting, transitions):
        self.states = states
        self.initial = initial
        self.accepting = accepting
        self.rejecting = rejecting
        self.transitions = transitions
        

    def run(self, input_tape):

        # check input and prepare initial config
        if input_tape == '':
            input_tape = '#'
        next_configs = [Config(1, self.initial, 0, list(input_tape))]
        print()

        # run simulation
        while True:

            # update config-lists
            current_configs = next_configs
            next_configs = []

            # print current configs
            for config in current_configs:
                print(config)
            print()
            print('-'*30)

            # continue or quit
            _ = input()
            if _ == "q" or _ == "Q":
                break

            # halting check
            all_done = True
            for config in current_configs:
                if config.state != self.accepting and config.state != self.rejecting:
                    all_done = False
                    break
            if all_done:
                break

            # apply transition function
            for config in current_configs:
                try:

                    # get transitions
                    transitions_for_current_config = self.transitions[config.state][config.tape[config.head]]

                    # compute new configs
                    for transition in transitions_for_current_config:

                        # change amplitude and state
                        new_config = Config(config.amp * transition[0], transition[1], config.head, list(config.tape))

                        # write
                        new_config.tape[new_config.head] = transition[2]

                        # move head
                        if transition[3] == '<':
                            if new_config.head == 0:
                                new_config.tape.insert(0, '#')
                            else:
                                new_config.head -= 1
                        elif transition[3] == '>':
                            if new_config.head == len(new_config.tape) - 1:
                                new_config.tape.append('#')
                            new_config.head += 1

                        # trim blanks
                        while new_config.tape[0] == '#' and new_config.head > 0:
                            new_config.tape.pop(0)
                            new_config.head -= 1
                        while new_config.tape[-1] == '#' and new_config.head < len(new_config.tape)-1:
                            new_config.tape.pop(-1)

                        # add config
                        next_configs.append(new_config)

                except KeyError:
                    break