rules = list(open('deterministic_tm.input', 'r'))

name = ""
initial = ""
accepting = ""
transitions = {}

current = None
for i in range(len(rules)):
    line = rules[i]
    if line.startswith("//"):
        continue
    elif line.startswith(("name")):
        name = line.replace("name:", "").strip()
    elif line.startswith(("init")):
        initial = line.replace("init:", "").strip()
    elif line.startswith(("accept")):
        accepting = line.replace("accept:", "").strip()
    else:
        x = line.strip().split(',')
        if len(x) == 2:
            current = (x[0],x[1])
        elif len(x) == 3:
            if current[0] not in transitions:
                transitions[current[0]] = dict()
            transitions[current[0]][current[1]] = (x[0],x[1],x[2])


_input = input().strip()
if _input == '':
    _input = '_'


tape = list(_input)
head = 0
state = initial

while state != accepting:
    try:
        transition = transitions[state][tape[head]]
    except KeyError:
        break
    state = transition[0]
    tape[head] = transition[1]
    if transition[2] == '<':
        if head == 0:
            tape.insert(0,'_')
        else:
            head -= 1
    elif transition[2] == '>':
        if head == len(tape)-1:
            tape.append('_')
        head += 1


print(*list(_input))
print(' ->')
print(*tape)
print()

if state == accepting:
    print('accepted')
else:
    print('rejected')
