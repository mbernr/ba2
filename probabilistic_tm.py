from random import random


rules = list(open('probabilistic_tm.input', 'r'))

name = ""
initial = ""
accepting = ""
rejecting = ""
transitions = {}

current = None
for i in range(len(rules)):
    line = rules[i]
    if line.startswith("//"):
        continue
    elif line.startswith(("name")):
        name = line.replace("name:", "").strip()
    elif line.startswith(("init")):
        initial = line.replace("init:", "").strip()
    elif line.startswith(("accept")):
        accepting = line.replace("accept:", "").strip()
    elif line.startswith(("reject")):
        accepting = line.replace("reject:", "").strip()
    else:
        x = line.strip().split(',')
        if len(x) == 2:
            current = (x[0],x[1])
        elif len(x) == 4:
            if current[0] not in transitions:
                transitions[current[0]] = dict()
            if current[1] not in transitions[current[0]]:
                transitions[current[0]][current[1]] = []
            transitions[current[0]][current[1]].append((float(x[0]),x[1],x[2],x[3]))


_input = input().strip()
if _input == '':
    _input = '#'


tape = list(_input)
head = 0
state = initial

print('.')
print(*list(_input))

while state != accepting:
    try:
        r = random()
        possible_transitions = transitions[state][tape[head]]
        for x in possible_transitions:
            if r <= x[0]:
                transition = x
                break
    except KeyError:
        break
    state = transition[1]
    tape[head] = transition[2]
    if transition[3] == '<':
        if head == 0:
            tape.insert(0,'#')
        else:
            head -= 1
    elif transition[3] == '>':
        if head == len(tape)-1:
            tape.append('#')
        head += 1

    print(' ->')
    print('{}.'.format(' '*head*2))
    print(*tape)


print()

if state == accepting:
    print('accepted')
else:
    print('rejected')
