class Config:

    def __init__(self, amp, state, head, tape):
        self.amp = amp
        self.state = state
        self.head = head
        self.tape = tape

    def __str__(self):
        text = ""
        text += "{:10.5f}  |  ".format(self.amp)
        text += "{}  |  ".format(self.state)
        for i in range(len(self.tape)):
            if i == self.head:
                text += "("
            text += self.tape[i]
            if i == self.head:
                text += ")"
        return text
